/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sarunpat.lab4;

/**
 *
 * @author USER
 */
import java.util.Scanner;

public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                isFinish = true;
            }
            if (table.checkDraw()) {
                printTable();
                printDraw();
                isFinish = true;
            }
            table.switchPlayer();
        }
        playAgain();
    }

    private void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void printTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!");
    }

    private void printDraw() {
        System.out.println("This game is Draw!!");
    }

    private void playAgain() {
        boolean correctInput = false;
        Scanner sc = new Scanner(System.in);
        while (!false) {
            System.out.print("Do you  wanna play again? (y/n) : ");
            char again = sc.next().charAt(0);
            if (again == 'y') {
                newGame();
                play();
                break;
            } else if (again == 'n') {
                System.out.print("Thanks for playing...Cya!");
                break;
            } else {
                System.out.println("Invalid input! Please input 'y' or 'n' . ");
            }
        }
    }
}
